% Prolog Exercises Part 2

% Ex 2.1

% size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.
% this is not fully relational because of the use of numbers (and so "is" operator)

% Ex 2.2

% size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
size_peano([],z).
size_peano([_|T],s(N)) :- size_peano(T, N).

% Ex 2.3

% sum(List, Sum)
sum([], 0).
sum([H|T], R) :- sum(T,S), R is H + S.

% Ex 2.4

% average(List, Average)
average(List, A) :- average(List,0,0,A).
average([], C, S, A) :- A is S/C.
average([X|Xs],C,S,A) :- 
				C2 is C+1,
				S2 is S+X,
				average(Xs,C2,S2,A).

% Ex 2.5

% max(List,Max)
% Max is the biggest element in List
max([], "NaN").
max([X|Xs], M) :- max([X|Xs], X, M).

max([], M, M).
max([X|Xs],C,M) :- 
				X > C,
				max(Xs,X,M).
max([X|Xs],C,M) :- 
				X =< C,
				max(Xs,C,M).
				