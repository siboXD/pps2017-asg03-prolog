% Prolog "Other" Exercises

% Ex 1

% inv(List,List)
% example: inv([1,2,3],[3,2,1]).

inv([],[]).
inv([X|Xs],L2):- inv(Xs,Sx), last(Sx,X,L2).

last([],X,[X]).
last([X|Xs],Y,[X|Ys]):- last(Xs,Y,Ys).

% Ex 2

% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).

double([],[]).
double(L,LTot):- attach(L,L,LTot).

attach([],[],[]).
attach([],[X|Xs],[X|Xs]).
attach([X|Xs],L2,[X|List]):- attach(Xs,L2,List).


% Ex 3

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

times(L,0,[]).
times(L,N,LTot):- N > 0,
									N2 is N-1,
									times(L,N2,LTot2),
									attach(L,LTot2,LTot).

% Ex 4

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).

proj([],[]).
proj([[X|Xs]|T], [X|Ys]):- proj(T,Ys).
