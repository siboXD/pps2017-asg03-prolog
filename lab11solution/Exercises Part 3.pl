% Ex 3

% � Implement predicate next/4 as follows
% � next(@Table,@Player,-Result,-NewTable)
% � Table is a representation of a TTT table where players X or O are playing
% � Player (either X or O) is the player to move
% � Result is either (either win(x), win(o), nothing, or even)
% � NewTable is the table after a valid move
% � Should find a representation for the Table
% � Calling the predicate should give all results
% � Secondarily, implement predicate:
% � game(@Table,@Player,-Result,-TableList)
% � TableList is the sequence of tables until Result win(x), win(o) or even

% Some hint
% � Choosing the right representation for a table is key
% � with a good representation it is easier to select the next move, and to check if somebody won
% � if needed, prepare to separate representation from visualisation
% � Possibilities
% � [[_,_,_],[x,o,x],[o,x,_]]: nice but advanced
% � [[n,n,n],[x,o,x],[o,x,n]]: compact, but need work
% � [cell(0,1,x),cell(1,1,o),cell(2,1,x),�]: easier
% � � do you have a different proposal?

player(o).
player(x).

next(Table,Player,Result,NewTable):- placeAnyMark(Table,Player,NewTable), result(NewTable, Result).

game(Table,Player,Result,TableList):- computeGames(Table,Player,Result,TableList).

% possible games computation
computeGames(TTT,_,R,[]):- result(TTT,R), gameEnded(R), !.
computeGames(TTT,P,R,[H|T]):- next(TTT,P,_,H),
															switchPlayer(P,P1),
															computeGames(H,P1,R,T).


% MARK PLACEMENT IN BOARD

% places a mark somewhere in the table
placeAnyMark([R|Rs],P,[NR|Rs]):- placeAnyMarkInRow(R,P,NR).
placeAnyMark([R|Rs],P,[R|NRs]):- placeAnyMark(Rs,P,NRs).

% places a mark of a player in a row
placeAnyMarkInRow([RH|RT],P,[P|RT]):- not(player(RH)).
placeAnyMarkInRow([RH|RT],P,[RH|NR]):- placeAnyMarkInRow(RT,P,NR).

% BOARD RESULT COMPUTATION

% computes the result of TicTacToe table
result(TTT,win(W)):- (checkRows(TTT,W);checkColumns(TTT,W);checkDiagonals(TTT,W)),
											player(W), !.
result(TTT,even):- not(placeAnyMark(TTT,r,_)), !.
result(TTT,nothing).

% check if row contains only one player's marks
checkRows([R|Rs],W):- allSame(R,W).
checkRows([_|Rs],W):- checkRows(Rs,W).

% check if a column contains only one player's mark
checkColumns(TTT,W):- checkFirstColumn(TTT,W);
											checkSecondColumn(TTT,W);
											checkThirdColumn(TTT,W).

% three different checks for each column
checkFirstColumn(TTT,W):- checkColumn(TTT,[],W).
checkSecondColumn(TTT,W):- removeFirstColumn(TTT,TTT2),
													checkColumn(TTT2,[],W).
checkThirdColumn(TTT,W):- removeFirstColumn(TTT,TTT2), 
													removeFirstColumn(TTT2,TTT3),
													checkColumn(TTT3,[],W).

% generic check that takes first column of board and checks if all marks are equal
checkColumn([],L,W):- allSame(L,W).
checkColumn([[RH|_]|Rs],L,W):- checkColumn(Rs,[RH|L],W).

% check diagonals to have same player's mark
checkDiagonals(TTT,W):- checkDiagonal(TTT,[],W);checkAntiDiagonal(TTT,[],W).

% check for diagonal
checkDiagonal([],L,W):- allSame(L,W).
checkDiagonal([[RH|_]|Rs],L,W):- removeFirstColumn(Rs,Rs2), checkDiagonal(Rs2,[RH|L],W).

% check for anti-diagonal
checkAntiDiagonal([],L,W):- allSame(L,W).
checkAntiDiagonal(TTT,L,W):- removeLastColumn(TTT,[_|Rs],[C|_]), checkAntiDiagonal(Rs,[C|L],W).


% GAME ENDING CHECK

gameEnded(win(o)).
gameEnded(win(x)).
gameEnded(even).

% UTILITIES

% removes the first board column from representation
removeFirstColumn([],[]).
removeFirstColumn([[_|RT]|Rs],[RT|Rs2]):- removeFirstColumn(Rs,Rs2).

% removes the last board column from representation, and returns it
removeLastColumn([],[],[]).
removeLastColumn([R|Rs],[R2|Rs2],[E|Es]):- removeLastElem(R,R2,E), removeLastColumn(Rs,Rs2,Es).

% removes and gets last element of list
removeLastElem([E],[],E).
removeLastElem([H|T],[H|T2],R):- removeLastElem(T,T2,R).

% checks all list elements the same
allSame([E],E).
allSame([E|Es],E):- allSame(Es,E).

% switches player
switchPlayer(x,o).
switchPlayer(o,x).

