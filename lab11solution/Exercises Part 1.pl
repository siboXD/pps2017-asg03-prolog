% Advanced Prolog Exercises

% dropAny(?Elem,?List,?OutList)
% Drops any occurrence of element, one at a time
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% Ex 1.1

% dropFirst(?Elem, +List, -OutList)
% drops only the first occurrence (showing no alternative results)
dropFirst(X,[X|T],T) :- !.
dropFirst(X,[Y|Ys],[Y|Zs]) :- dropFirst(X,Ys,Zs).

% dropLast(+Elem, +List, -OutList)
% drops only the last occurrence (showing no alternative results)
dropLast(X,[X|T],T) :- not(member(X,T)), !.
dropLast(X,[H|T],[H|Ys]) :- dropLast(X,T,Ys).

% dropAll(+Elem, +List, -OutList)
% drop all occurrences, returning a single list as result
dropAll(X,L,L) :- not(member(X,L)), !.
dropAll(X,[XCopy|T],Ys) :- copy_term(X,XCopy), dropAll(X,T,Ys), !.
dropAll(X,[H|T],[H|Ys]) :- dropAll(X,T,Ys).
