% Advanced Prolog Exercises

% Ex 2.1

% fromList(+List,-Graph)
% It obtains a graph from a list
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% Ex 2.2

% fromCircList(+List,-Graph)
% Obtain a graph from a circular list

fromCircList([X],[e(X,X)]).
fromCircList([H1|T],L):- fromCircList([H1|T],H1,L).

fromCircList([X],First,[e(X,First)]).
fromCircList([H1,H2|T],First,[e(H1,H2)|L]):- fromCircList([H2|T],First,L).

% Ex 2.3

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node

% use dropAll defined in 1.1
dropAll(X,L,L) :- not(member(X,L)), !.
dropAll(X,[XCopy|T],Ys) :- copy_term(X,XCopy), dropAll(X,T,Ys), !.
dropAll(X,[H|T],[H|Ys]) :- dropAll(X,T,Ys).

dropNode(G,N,O):- dropAll(e(N,_),G,G2),
									dropAll(e(_,N),G2,O).

% Ex 2.4

% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined 
% with member(?Elem,?List)

% first version
% reaching([],_,[]).
% reaching([e(E,R)|T],E,[R|T2]):- !, reaching(T,E,T2).
% reaching([H|T],E,T2):- reaching(T,E,T2).

% second version (with findall)
reaching([],_,[]).
reaching(L,E,R):- findall(X,member(e(E,X),L),R).

% Ex 2.5

% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1

% first version
% test goal: anypath([e(1,2),e(1,3),e(2,3)],1,3,L).

% anypath([],_,_,[]).
% anypath(G,N1,N2,[e(N1,N2)]):- member(e(N1,N2),G).
% anypath(G,N1,N2,[e(N1,N3)|Es]):- member(e(N1,N3),G), anypath(G,N3,N2,Es).

% second version (working with cyclic graphs)
% test goal: anypath([e(1,2),e(1,3),e(2,3),e(3,4),e(2,4),e(4,1)],1,4,L).

% member2(List, Elem, ListWithoutElem)
member2([X|Xs],X,Xs).
member2([X|Xs],E,[X|Ys]):-member2(Xs,E,Ys).

anypath([],_,_,[]).
anypath(G,N1,N2,[e(N1,N2)]):- member(e(N1,N2),G).
anypath(G,N1,N2,[e(N1,N3)|Es]):- member2(G,e(N1,N3),G2), anypath(G2,N3,N2,Es).

% Ex 2.6

% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!

% test goal: allreaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).

allreaching([],_,[]).
allreaching(G,N,L4):- findall(X1,anypath(G,N,_,X1),L1),
											lastOfEachList(L1,L2),
											findall(X2,member(e(_,X2),L2),L3),
											setof(X3,member(X3,L3),L4), !.

% lastOfEachList(ListOfLists, ListOfLastElements)
lastOfEachList([],[]).
lastOfEachList([[E]|Ls],[E|Es]):- lastOfEachList(Ls,Es).
lastOfEachList([[_|T]|Ls],Es):- lastOfEachList([T|Ls],Es).
